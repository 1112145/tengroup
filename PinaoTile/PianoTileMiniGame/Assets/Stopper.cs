﻿using UnityEngine;
using System.Collections;

public class Stopper : MonoBehaviour {

	[HideInInspector]
	public string lastCollision;
	public SpinTargetController controller;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	int count = 0;
	void OnCollisionEnter2D(Collision2D coll) {
		//controller.player.Play ();
		if (count < 11) {
			//Debug.Log (coll.gameObject.name);
		}
		lastCollision = coll.gameObject.name;
		SpinTargetController.target = SpinTargetController.elements [int.Parse (lastCollision)].value;
		//controller.txtTarget.text = SpinTargetController.elements [int.Parse (lastCollision)].value.ToString ();
		//Debug.Log (coll.gameObject.name);
		count++;
	}
}
