﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum GAMESTATE
{
	PAUSE,
	PLAY,
	FINISHED
}

public class DemoBanTrung : MonoBehaviour {

	public Timer timer;
	public GameObject gun;
	public GameObject bullet;
	public GameObject btnPlay;
	public Text txtScore;
	public Text txtDialogTitle;
	private GameObject currentBullet;
	public GameObject nongNgam;

	private Vector3 orgPos;

	public GameObject balloonContainer;
	public Balloon balloon;
	Rect containerSize;
	Rect balloonSize;
	private float time = 0;
	private float timeLimit = 3f;

	[HideInInspector]
	public int score = 0;

	Vector3 screenPos;
	Vector3 worldPos;
	bool beDrag = false;
	//public GameObject Bottle;

	private List<float> xPos = new List<float>();
	public GAMESTATE state = GAMESTATE.PAUSE;

	private AudioSource[] auSrcs;

	// Use this for initialization
	void Start () {
		Screen.orientation = ScreenOrientation.Portrait;
		currentBullet = bullet;
		containerSize = ((RectTransform)balloonContainer.transform).rect;
		balloonSize = ((RectTransform)balloon.transform).rect;
		for (int i = 0; i < containerSize.width/balloonSize.width - 1; i++) {
			xPos.Add(i*balloonSize.width);
		}
		auSrcs = GetComponents<AudioSource> ();
		//auSrcs [0].Play ();
	}


	public void RenderScore()
	{
		txtScore.text = score.ToString ();
	}


	bool changedSpeed = false;
	bool ShowDialog = false;
	// Update is called once per frame
	void Update () {
		if (state == GAMESTATE.PLAY) {
			if(timer.CurrentValue != 60 && timer.CurrentValue % 20 == 0)
			{
				if(!changedSpeed){
					balloon.speed += 1;
					changedSpeed = true;
					Debug.Log("Change speed!");
				}
			}
			else
			{
				changedSpeed = false;
			}

			RenderScore ();
			time += Time.deltaTime;
			if (time >= timeLimit) {
				time = 0;
				NewBalloons ();
			}

			MouseController ();

			#if UNITY_ANDROID
			TouchControllBottle ();
			#endif
			if(timer.currentState == Timer.TimerState.Stop)
			{
				state = GAMESTATE.FINISHED;
				if(ShowDialog == false)
				{
					ShowDialogResult();
					ShowDialog = true;
				}
			}
		}
	}
	bool isMouseDown = false;
	void MouseController()
	{
		Vector3 screenPos = Input.mousePosition;
		if(Input.GetMouseButtonDown(0))
		{
			isMouseDown = true;

		}
		if(isMouseDown)
		{
			nongNgam.SetActive(true);
			worldPos = Camera.main.ScreenToWorldPoint (screenPos);
			RotateGun (worldPos);
			if(Input.GetMouseButtonUp(0))
			{
				nongNgam.SetActive(false);
				shoot = true;
				isMouseDown = false;
			}
		}
	}

	void TouchControllBottle ()
	{
		if (Input.touchCount != 1) {
			beDrag = false;
			return;
		}
		Touch touch = Input.touches [0];
		Vector3 screenPos = touch.position;
		if (touch.phase == TouchPhase.Began) {
			beDrag = true;
		}
		if (beDrag && touch.phase == TouchPhase.Moved) {
			nongNgam.SetActive(true);
			worldPos = Camera.main.ScreenToWorldPoint (screenPos);
			RotateGun (worldPos);
		}
		if (beDrag && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)) {
			shoot = true;
			nongNgam.SetActive(false);
			beDrag = false;
		}
	}

	void RotateGun (Vector3 worldPos)
	{
		gun.transform.rotation = Quaternion.LookRotation(Vector3.forward, worldPos - gun.transform.position);
	}

	bool shoot = false;
	[HideInInspector]
	public bool balloonBroken = false;
	[HideInInspector]
	public bool balloonRight = false;
	[HideInInspector]
	public bool balloonWrong = false;

	void FixedUpdate()
	{
		if (balloonBroken) {
			auSrcs[0].Play();
			balloonBroken = false;
		}
		if (balloonRight) {
			auSrcs[1].Play();
			balloonRight = false;
		}
		if (balloonWrong) {
			auSrcs[2].Play();
			balloonWrong = false;
		}
		if (state == GAMESTATE.PLAY) {
			if (shoot) {
				Shoot ();
				shoot = false;
			}
		}
	}


	void Shoot ()
	{
		currentBullet.SetActive (true);
		Rigidbody2D rigid = currentBullet.GetComponent<Rigidbody2D> ();
		Vector3 cachedDirection = new Vector3 (gun.transform.up.x, gun.transform.up.y, gun.transform.up.z);
		rigid.AddForce(cachedDirection * 2000.0f);
		Debug.Log ("Shoot");
		StartCoroutine (NewBullet ());
	}
	
	IEnumerator NewBullet()
	{
		yield return new WaitForSeconds(0.02f);
		GameObject newBullet = GameObject.Instantiate (bullet);
		newBullet.transform.SetParent (GameObject.Find("BalloonPanel").transform);
		newBullet.transform.localScale = new Vector3 (1, 1, 1);
		newBullet.transform.position = gun.transform.position + 0.5f*gun.transform.up;
		//newBullet.transform.rotation = gun.transform.rotation;
		currentBullet = newBullet;
		currentBullet.SetActive (false);
		
	}
	
	void NewBalloons ()
	{
		int numberBalloon = Random.Range (2, 4);
		for (int i = 0; i < numberBalloon; i++) {
			NewABalloon ();
		}

	}

	public void PlayGame()
	{
		state = GAMESTATE.PLAY;
		NewBalloons();
		timer.StartTimer ();
		btnPlay.SetActive (false);
	}

	public void RePlay()
	{
		Application.LoadLevel (Application.loadedLevelName);
	}

	int xLocationNewIndex;
	int xLocationOld = -1;
	void NewABalloon ()
	{
		GameObject newBalloon = GameObject.Instantiate (balloon.gameObject);
		newBalloon.transform.SetParent (balloonContainer.transform);
		newBalloon.transform.localScale = new Vector3 (1, 1, 1);
		do{
			xLocationNewIndex = Random.Range (0, xPos.Count);
		}
		while (xLocationNewIndex == xLocationOld);

		newBalloon.transform.localPosition = balloon.transform.localPosition + new Vector3 (xPos[xLocationNewIndex], 0, 0);
		xLocationOld = xLocationNewIndex;
		Balloon script = newBalloon.GetComponent<Balloon> ();
		script.RandomBalloon ();
		script.speed = balloon.speed;
		Debug.Log ("speed: " + balloon.speed);
		script.state = Balloon.BalloonState.fall;
	}

	public GameObject hiden;
	public GameObject dialog;

	public void ShowDialogResult()
	{
		hiden.SetActive (true);

		if (score >= SpinTargetController.target) {
			txtDialogTitle.text = "YOU WIN";
			Image image = dialog.GetComponent<Image>();
			image.sprite = Resources.Load<Sprite>("Images/BanTrung/game over-01");
			auSrcs[3].Play();
		} else {
			txtDialogTitle.text = "YOU LOSE";
			Image image = dialog.GetComponent<Image>();
			image.sprite = Resources.Load<Sprite>("Images/BanTrung/game over-03");
			auSrcs[4].Play();
		}
		UITransition.PopIn (dialog.transform);
	}

	public void BackToWheel()
	{
		Application.LoadLevel ("WheelSpin");
	}
}


