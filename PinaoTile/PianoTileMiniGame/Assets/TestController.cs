﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TestController : MonoBehaviour {

	public Text txtTouchPosition;
	public GameObject gun;
	public GameObject bullet;
	public GameObject currentBullet;
	private Vector3 orgPos;
	// Use this for initialization
	void Start () {
		currentBullet = bullet;
		orgPos = bullet.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.touchCount != 1) {
			beDrag = false; 
			return;
		}

		Touch touch = Input.touches[0];
		Vector3 screenPos = touch.position;

		if (touch.phase == TouchPhase.Began) {
			beDrag = true;
		}
		if (beDrag && touch.phase == TouchPhase.Moved) {
			worldPos = Camera.main.ScreenToWorldPoint(screenPos);
			RotateGun(worldPos);
		}
		if (beDrag &&(touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)) {
			shoot = true;
			beDrag = false;
		}

	}

	bool shoot = false;

	void FixedUpdate()
	{
		if (shoot) {
			Shoot ();
			shoot = false;
		}
	}


	bool isMouseDown = false;
	bool isMouseUp = false;

	void RotateGun (Vector3 worldPos)
	{
		gun.transform.rotation = Quaternion.LookRotation(Vector3.forward, worldPos - gun.transform.position);
	}
	Vector3 screenPos;
	Vector3 worldPos;
	bool beDrag = false;
	public void OnMouseDown()
	{
		if (Input.GetMouseButtonDown (0)) {
			isMouseDown = true;
			Debug.Log("MouseDOwn!");
		}
		if (isMouseDown) {
			screenPos = Input.mousePosition;
			worldPos = Camera.main.ScreenToWorldPoint(screenPos);
			RotateGun(worldPos);
		}
	}

	public void OnMouseUp()
	{
		if (isMouseDown) {
			if (Input.GetMouseButtonUp (0)) {
				Shoot();
				isMouseDown = false;
				Debug.Log("Mouse Up!");
			}
		}
	}

	void Shoot ()
	{
		Rigidbody2D rigid = currentBullet.GetComponent<Rigidbody2D> ();
		Vector3 cachedDirection = new Vector3 (gun.transform.up.x, gun.transform.up.y, gun.transform.up.z);
		rigid.AddForce(cachedDirection * 1500.0f);
		Debug.Log ("Shoot");
		StartCoroutine (NewBullet ());
	}

	IEnumerator NewBullet()
	{
		yield return new WaitForSeconds(0.02f);
		GameObject newBullet = GameObject.Instantiate (bullet);
		newBullet.transform.SetParent (gun.transform);
		newBullet.transform.localScale = new Vector3 (1, 1, 1);
		newBullet.transform.position = gun.transform.position + 4*gun.transform.up;
		//newBullet.transform.rotation = gun.transform.rotation;
		currentBullet = newBullet;
	}
}
