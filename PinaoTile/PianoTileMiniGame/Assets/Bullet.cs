﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.name=="balloon" || coll.gameObject.name=="balloon(Clone)")
		{
			if(this.gameObject.name != "bullet")
				this.gameObject.SetActive(false);
		}
	}
}
