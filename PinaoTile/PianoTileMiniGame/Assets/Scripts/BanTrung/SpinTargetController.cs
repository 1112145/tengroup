﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SpinTargetController : MonoBehaviour {

	public Stopper stopper;
	public AnimationCurve curves;
	public Image wheel;
	public static List<WheelElement> elements = new List<WheelElement> ();

	public static int target = 0;

	private bool spinning = false;
	private bool clickSpin = false;

	[HideInInspector]
	public AudioSource player;
	public Text txtTarget;
	// Use this for initialization
	void Start () {
		player = GetComponent<AudioSource> ();
		target = 0;
		Screen.orientation = ScreenOrientation.Portrait;
		spinning = false;
		InitElements ();
	}
	
	// Update is called once per frame
	void Update () {
		if (clickSpin == true && !spinning) {
			StartCoroutine(DoSpin(5.0f, Random.Range(2000,3000)));
		}
	}


	public void Spin()
	{
		clickSpin = true;
		player.Play ();
	}
	public GameObject GO;
	public IEnumerator DoSpin(float time, float angle)
	{
		spinning = true;
		float timer = 0;
		float startAngle = wheel.gameObject.transform.eulerAngles.z;
		//Debug.Log(wheel.gameObject.transform.eulerAngles.z);
		while (timer < time) {
			float endAngle = curves.Evaluate(timer/time) * angle;
			wheel.transform.eulerAngles = new Vector3(0,0, endAngle + startAngle);
			//Debug.Log(wheel.gameObject.transform.eulerAngles.z);
			timer+= Time.deltaTime;
			yield return 0;
		}
		player.Stop ();
		spinning = false;
		clickSpin = false;
		int index = int.Parse (stopper.lastCollision);
		GO.SetActive (true);
		GameObject cotmoc = GameObject.Find (index.ToString ());

		//Debug.Log (elements[index].value.ToString());
	}

	public class WheelElement
	{
		public string id;
		public int value;
		public WheelElement(string id, int value)
		{
			this.id = id;
			this.value = value;
		}
	}

	public void GOTOMAINGAME()
	{
		Application.LoadLevel ("MainGame");
	}

	public static void InitElements()
	{
		elements.Add (new WheelElement ("0",250));
		elements.Add (new WheelElement ("1",350));
		elements.Add (new WheelElement ("2",200));
		elements.Add (new WheelElement ("3",400));
		elements.Add (new WheelElement ("4",200));
		elements.Add (new WheelElement ("5",400));
		elements.Add (new WheelElement ("6",150));
		elements.Add (new WheelElement ("7",350));
		elements.Add (new WheelElement ("8",150));
		elements.Add (new WheelElement ("9",300));

	}

	//public int FindElements(float 

}