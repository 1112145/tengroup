﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Balloon : MonoBehaviour {

	public GameObject parent;
	public DemoBanTrung controller;

	public enum BalloonType
	{
		UseFul,
		HarmFul
	}

	public enum BalloonState
	{
		idle,
		fall,
		explosion
	}

	[HideInInspector]
	public BalloonType balloonType;
	[HideInInspector]
	public string balloonName;
	[HideInInspector]
	public Sprite balloonSprite;

	public BalloonState state = BalloonState.idle;
	//[HideInInspector]
	public float speed= 2f;




	public void SetBalloon(BalloonType type, string name, BalloonState state, Sprite sprite)
	{
		this.balloonName = name;
		this.balloonSprite = sprite;
		this.balloonType = type;
		this.state = state;
	}


	// Use this for initialization
	void Start () {
		//speed = 2f;
	}

	void Fall ()
	{
		if (this.transform.position.y < parent.transform.position.y) {
			if(this.balloonType == BalloonType.HarmFul)
			{
				controller.score -= 20;
				controller.balloonWrong = true;
			}
			Destroy(this.gameObject);
		}
		this.transform.localPosition -= new Vector3 (0, speed, 0);
	}

	bool changeSpeed = false;
	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate()
	{
		if (state == BalloonState.idle) {
			CircleCollider2D collider = GetComponent<CircleCollider2D>();
			collider.enabled = false;
		}
		if (state == BalloonState.fall && controller.state == GAMESTATE.PLAY) {
			CircleCollider2D collider = GetComponent<CircleCollider2D>();
			collider.enabled = true;
			Fall();
		}
		if (state == BalloonState.explosion) {
			this.gameObject.SetActive(false);
		}

	}
	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.name=="bullet" || coll.gameObject.name=="bullet(Clone)")
		{
			if (state == BalloonState.idle) {

			}
			else if(state == BalloonState.fall)
			{
				state = BalloonState.explosion;
				//StartCoroutine(PlaySoundBroken());
				controller.balloonBroken = true;
				Debug.Log("BOOM!");
				if(balloonType == BalloonType.HarmFul)
				{
					controller.score+= 20;
					controller.balloonRight = true;
				}
				else
				{
					controller.score-= 10;
					controller.balloonWrong = true;
				}
				Debug.Log("Score: " + controller.score);
			}
		}
	}



	public void RandomBalloon()
	{
		Sprite sprite;
		int index = Random.Range (0, 8);
		switch(index)
		{
			case 0:
				sprite = Resources.Load<Sprite>("Images/BanTrung/game-ban bongbon-02");
			SetBalloon(BalloonType.HarmFul,"Hương liệu",BalloonState.idle,sprite);
				break;
			case 1:
			sprite = Resources.Load<Sprite>("Images/BanTrung/game-ban bongbon-03");
			SetBalloon(BalloonType.HarmFul,"Xà phòng",BalloonState.idle,sprite);
				break;
			case 2:
			sprite = Resources.Load<Sprite>("Images/BanTrung/game-ban bongbon-04");
			SetBalloon(BalloonType.HarmFul,"Khô da",BalloonState.idle,sprite);
				break;
			case 3:
			sprite = Resources.Load<Sprite>("Images/BanTrung/game-ban bongbon-05");
			SetBalloon(BalloonType.HarmFul,"Kích ứng da",BalloonState.idle,sprite);
				break;
			case 4:
			sprite = Resources.Load<Sprite>("Images/BanTrung/game-ban bongbon-06");
			SetBalloon(BalloonType.UseFul,"Làm sạch",BalloonState.idle,sprite);
				break;
			case 5:
			sprite = Resources.Load<Sprite>("Images/BanTrung/game-ban bongbon-07");
			SetBalloon(BalloonType.UseFul,"Dịu nhẹ",BalloonState.idle,sprite);
				break;
			case 6:
			sprite = Resources.Load<Sprite>("Images/BanTrung/game-ban bongbon-08");
			SetBalloon(BalloonType.UseFul,"Tắm khô",BalloonState.idle,sprite);
				break;
			case 7:
			sprite = Resources.Load<Sprite>("Images/BanTrung/game-ban bongbon-09");
			SetBalloon(BalloonType.UseFul,"Bảo vệ",BalloonState.idle,sprite);
				break;
		}
		this.GetComponent<Image> ().sprite = this.balloonSprite;
	}


}
