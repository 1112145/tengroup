﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class TestControllet : MonoBehaviour {

	#region PrivateField
	private Song song;
	private List<AudioClip> notes;
	private AudioSource[] myAs;
	private string[] musics;
	private int currentNotes = 0;
	private int index = 0;

	private List<string[]> mainNote = new List<string[]>();
	private List<string[]> chordNote = new List<string[]>();
	#endregion

	// Use this for initialization
	void Start () {
		// Load a JSON that config the song
		//LoadSongFromJSON ("LittleStar");
		//LoadSongFromJSON ("Arirang");
		// Load all music notes mp3 files.
		//LoadMusicNotes ();


		//TranformToMusicArray ();
		//StartCoroutine (ChoiMotDoan());
		//StartCoroutine (PlayWholeSong());

	}

	public void InitSong(string name)
	{
		LoadSongFromJSON (name);
		LoadMusicNotes ();
		TranformToMusicArray ();
		StartCoroutine (PlayWholeSong());
	}

	// Update is called once per frame
	void Update () {
		
	}

	#region Core function
	public void LoadSongFromJSON (string name)
	{
		string filePath = "JSON/" + name;
		myAs = GetComponents<AudioSource> ();
		TextAsset targetFile = Resources.Load<TextAsset> (filePath);
		song = JsonConvert.DeserializeObject<Song> (targetFile.text);
	}

	public void LoadMusicNotes()
	{
		notes = new List<AudioClip> ();
		Object[] audios = Resources.LoadAll("Sounds/Musics/");
		for (int i = 0; i < audios.Length; i++) {
			AudioClip ac = audios[i] as AudioClip;
			notes.Add(ac);
		}
	}

	public float PlayMusic(string s)
	{
		string time;
		float t;
		if (s.Contains (".")) {
			string[] parts = s.Split(')');
			string[] chordrus = (parts[0].Replace("(","")).Split('.');
			for (int i = 0; i < chordrus.Length; i++) {
				myAs[i+5].clip = FindAudioClipByNoteName(chordrus[i]);
				StartCoroutine(playSound(myAs[i+2]));
			}
			time = parts[1].Replace("[","").Replace("]","");
			Debug.Log(s + ">>" +time);
			
		} else if (s.Contains ("[") || s.Contains("]")) {
			string[] temp = (s.Replace("]","")).Split('[');
			time = temp[1];
			Debug.Log(s + ">>" +time);
			myAs[index].clip = FindAudioClipByNoteName(temp[0]);
			StartCoroutine(playSound(myAs[index]));
			index++;
			if(index > 4)
			{
				index = 0;
			}
		} else {
			if(s == "U")
			{
				Debug.Log("U");
			}
			time = "L";
		}
		if (time == "J") {
			return 1.75f;
		} else if (time == "K") {
			return 0.7f;
		}else {
			return 0.35f;
		}
	}

	private AudioClip FindAudioClipByNoteName(string s)
	{
		for (int i = 0; i < notes.Count; i++) {
			if(notes[i].name.Equals(s,System.StringComparison.Ordinal)){
				return notes[i];
			}
			
		}
		return null;
	}

	IEnumerator playSound(AudioSource myAs)
	{
		yield return new WaitForSeconds (0.0000001f);
		myAs.Play();
	}
	#endregion


	public List<Unit> unitsSound = new List<Unit>();
	public class Unit
	{
		public string leftHand;
		public string rightHand;
		public Unit(string leftHand, string rightHand)
		{
			this.leftHand = leftHand;
			this.rightHand = rightHand;
		}
		public Unit()
		{

		}
		public Unit(Unit unit)
		{
			this.leftHand = unit.leftHand;
			this.rightHand = unit.rightHand;
		}
	}
	void TranformToMusicArray ()
	{
		string leftHand = "";
		string RightHand = "";
		for (int i = 0; i < 3; i++) {
			leftHand+= song.musics[i].scores[0];
			RightHand+= song.musics[i].scores[1];
		}
		string[] leftArray = leftHand.Split (';');
		string[] rightArray = RightHand.Split (';');
		for (int i = 0; i < leftArray.Length - 1; i++) {
			string[] mainNotes = leftArray[i].Split(',');
			string[] chordNotes = rightArray[i].Split(',');
			mainNote.Add(mainNotes);
			chordNote.Add(chordNotes);
		}
	}




	private int mainIndex = 0;
	private int chordIndex = 0;
	private int currentMusic = 0;
	private bool TFlag = false;

	private int count = 0;

	public void OnclickPlayMusicButton()
	{
		if(count >= unitsSound.Count)
		{
			count = 0;
		}
		PlayOneUnit (unitsSound [count].leftHand, unitsSound [count].rightHand);
		count++;
	}


	IEnumerator ChoiMotDoan()
	{
		yield return new WaitForSeconds (0.35f);
		StartCoroutine(PlayLeftHand ());
		//yield return new WaitForSeconds (0.35f);
		StartCoroutine (PlayChord ());
	}

	IEnumerator PlayLeftHand ()
	{
		PlayMusic("c2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("c2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("d2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("(e2.g2)[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("(c2.d2.g2)[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");

		yield return new WaitForSeconds (0.35f);
		PlayMusic("a2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("a2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("c2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("g2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");

		yield return new WaitForSeconds (0.35f);
		PlayMusic("f2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("f2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("(c2.e2)[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("e2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");


	}

	IEnumerator PlayChord ()
	{
		PlayMusic("c1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("g1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("T");
		yield return new WaitForSeconds (0.7f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("g1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("g1[L]");

		PlayMusic("c1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("f1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("a1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("g1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("c1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("e1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("g1[L]");

		yield return new WaitForSeconds (0.35f);
		PlayMusic("d1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("a1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("b1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("g1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("U");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("g1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("a1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("e1[L]");
	}


	public void PlayOneUnit(string leftHand, string chord)
	{
		PlayMusic (leftHand);
		PlayMusic (chord);
	}


	IEnumerator PlayDemoSentence()
	{
		yield return new WaitForSeconds (0.35f);
		PlayMusic("c2[L]");
		PlayMusic("c1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("g1[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("c2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("d2[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("(e2.g2)[L]");
		PlayMusic("g1[L]");
		yield return new WaitForSeconds (0.35f);

		yield return new WaitForSeconds (0.35f);
		PlayMusic("(c2.d2.g2)[L]");
		yield return new WaitForSeconds (0.35f);
		PlayMusic("g1[L]");
//		PlayMusic("c1[L]");
//		yield return new WaitForSeconds (0.35f);
//		PlayMusic("f1[L]");
//		PlayMusic("a2[L]");
//		yield return new WaitForSeconds (0.35f);
//		PlayMusic("a1[L]");
//		yield return new WaitForSeconds (0.35f);
//		PlayMusic("a2[L]");
//		yield return new WaitForSeconds (0.35f);
//		PlayMusic("c2[L]");
//		yield return new WaitForSeconds (0.35f);
//		PlayMusic("g1[L]");
	}
	#region PlayAllSong
	IEnumerator PlayWholeSong()
	{
		Unit unit = new Unit();
		bool flagT = false;
		for (int i = 0; i < mainNote.Count; i++) {
			flagT = false;
			int idx = 0;
			for (int j = 0; j < mainNote[i].Length; j++) {
				//unit = new Unit ();
				//float t = PlayMusic(mainNote[i][j]);
				unit.leftHand = mainNote[i][j];

				if(chordNote[i][idx] == "T")
				{

					flagT = true;
					//PlayMusic(chordNote[i][idx]);
					unit.rightHand = chordNote[i][idx];
					//yield return new WaitForSeconds(0.7f);
					idx--;
				}
				else
				{
					if(flagT == false)
					{
						//PlayMusic(chordNote[i][j]);
						unit.rightHand = chordNote[i][j];
					}
					else
					{
						//PlayMusic(chordNote[i][idx]);
						unit.rightHand = chordNote[i][idx];
					}

				}
				unitsSound.Add(new Unit(unit));
				idx++;
				//yield return new WaitForSeconds(t);
				//yield return new WaitForSeconds(0.000001f);

			}
		}
		yield return null;
	}
	#endregion


	#region PUBLIC CLASSES MODEL
	public class Song
	{
		public int baseBpm;
		public Music[] musics = new Music[3];
	}
	
	public class Music
	{
		public int id;
		public float baseBeats;
		public string[] scores = new string[2];
	}
	#endregion
}
