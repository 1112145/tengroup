﻿using UnityEngine;
using System.Collections;

public class SoundPlayer : MonoBehaviour {

	[HideInInspector]
	public AudioSource[] audioSource;
	// Use this for initialization
	void Start () {
		audioSource = this.GetComponents<AudioSource> ();

	}

	public void PlayAudio(AudioClip audio)
	{
		audioSource[0] = this.GetComponent<AudioSource> ();
		audioSource[0].clip = audio;
		audioSource[0].Play ();
	}

	public void PlayOnClickButton()
	{
		AudioClip clip = Resources.Load<AudioClip> ("Sounds/OneCrown");
		audioSource [0].Play ();
	}

	public void RepeatAudio()
	{
		audioSource[0].Play ();
	}



	public void PlaySecondarySound(AudioClip audio)
	{
		audioSource [1].Play ();
	}
	// Update is called once per frame
	void Update () {
	
	}
}
