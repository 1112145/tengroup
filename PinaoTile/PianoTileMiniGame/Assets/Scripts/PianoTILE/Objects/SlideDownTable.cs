﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlideDownTable : MonoBehaviour {

	private const int LIMIT_BOTTOM = -10;
	private int CURRENT_PANEL = 2;
	public enum State
	{
		enable,
		disble
	}
	public List<GameObject> childElement;

	private float speed = 15.0f;
	private Vector3 orgPosition;
	private Vector3 temp;
	float deltaY;

	public State state;
	// Use this for initialization
	void Start () {
		//state = State.enable ;
		deltaY = childElement [2].transform.position.y - childElement [1].transform.position.y;
		Debug.Log (deltaY);

		temp = this.transform.localPosition;
		orgPosition = new Vector3(childElement [0].transform.position.x,childElement [0].transform.position.y,childElement [0].transform.position.z);

	}

	bool flag = false;

	// Update is called once per frame
	void Update () {

		if (state == State.enable) {
			this.transform.localPosition = new Vector3(temp.x,this.transform.localPosition.y - speed,temp.z);
		}
		// Kiem tra panel nam duoi cung co qua man hinh viewport chua?
		if(childElement[CURRENT_PANEL].transform.position.y <= LIMIT_BOTTOM)
		{

			Debug.Log(CURRENT_PANEL);
			childElement[CURRENT_PANEL].transform.position = new Vector3(orgPosition.x,orgPosition.y + deltaY ,orgPosition.z);
			CURRENT_PANEL--;
			if(CURRENT_PANEL < 0)
			{
				CURRENT_PANEL = 2;
			}
			// Copy cai component nay va dat len dau chinh no
//			SlideDownTable newItem = GameObject.Instantiate(this);
//			newItem.transform.localPosition = orgPosition;
		}

	
	}


}
