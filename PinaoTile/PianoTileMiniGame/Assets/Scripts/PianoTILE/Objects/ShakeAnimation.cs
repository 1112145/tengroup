﻿using UnityEngine;
using System.Collections;
using System;

public class ShakeAnimation : MonoBehaviour {

	private bool flagShake = false;
	public float timeToStopShake = 3f;
	public float f = 0.02f;
	int x = 0;
	float time = 0;
	float count = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (flagShake) {
			count += Time.deltaTime;
			if (count < timeToStopShake) {
				time += Time.deltaTime;
				if (time >= f) {
					x++;
					transform.localRotation = new Quaternion (0f, 0f, 0.03f * (float)Math.Sin (x), 1f);
					time = 0;
				}
			}
			else
			{
				flagShake = false;
				count = 0;
			}
		}
	}

	#region Shake Animation
	public void Shake()
	{
		flagShake = true;
	}
	
	public void StopShake()
	{
		flagShake = false;
	}
	
	#endregion
}
