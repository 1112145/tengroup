﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

	public SoundPlayer player;
	AudioClip sound;

	public int InitSecond;
	// Thoi gian hien tai;
	public int CurrentValue;
	// Text quan ly gia tri cua dong ho
	public Text txtValue;
	
	private float timeMax = 1.0f;
	
	private float timerCount = 0f;
	
	
	public enum TimerState
	{
		Run,
		Pause,
		Stop
	};
	
	public TimerState currentState = TimerState.Pause;
	
	public void ResetCountdownTimer()
	{
		InitSecond = CurrentValue = 0;
		txtValue.text = "0";
		currentState = TimerState.Stop;
	}
	public void StartTimer()
	{
		//InitSecond = 5;
		CurrentValue = InitSecond;
		txtValue.text = "0";
		currentState = TimerState.Run;
	}
	// Use this for initialization
	void Start () {
		//StartTimer ();
		sound = Resources.Load<AudioClip> ("Sounds/timerPlay");
	}
	
	// Update is called once per frame
	void Update () {
		switch(currentState){
		case TimerState.Run:
			// Chay dong ho
			RunTimer();
			break;
		case TimerState.Stop:
			// Dung dong ho. Reset timer lai
			StopTimer();
			break;
		case TimerState.Pause:
			// Tam ngung dong ho. Khong lam gi het
			break;
		}
	}
	bool play = false;
	public void RunTimer()
	{
		// Sau moi giay giam gia tri xuong 1
		if (EnoughOneSecond ())
		{
			CurrentValue--;
		}
		// Cap nhat gia tri len Textbox
		txtValue.text = "" + CurrentValue.ToString();
		
		if(CurrentValue <= 0)
		{
			currentState = TimerState.Stop;
			player.audioSource[0].Stop();
		}

		if(CurrentValue <= 5)
		{
			txtValue.color = Color.red;
			if(!play){
				player.PlayAudio(sound);
				play = true;
			}
		}
			
	}
	
	public bool EnoughOneSecond ()
	{
		timerCount += Time.deltaTime;
		if (timerCount > timeMax)
		{
			timerCount = 0;
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	public void StopTimer()
	{
		ResetCountdownTimer ();
	}
}
