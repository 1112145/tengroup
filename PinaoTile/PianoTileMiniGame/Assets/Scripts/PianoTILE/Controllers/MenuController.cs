﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {



	public GameObject hideBackGround;
	public GameObject dialogFinishGame;


	// Use this for initialization
	void Start () {
		Screen.orientation = ScreenOrientation.Landscape;
		soundPlayer = GetComponent<SoundPlayer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			ShowFinishDiaLog(true);
		}
	}
	SoundPlayer soundPlayer;
	public void PlayGame()
	{

		soundPlayer.PlayOnClickButton ();
		Application.LoadLevel ("GamePlayLandScape");
		//StartCoroutine (changeScene ());
	}

	IEnumerator changeScene()
	{
		yield return new WaitForSeconds (0.25f);
		Application.LoadLevel ("GamePlayLandScape");
	}

	public void ShowFinishDiaLog(bool show)
	{
		if (show) {
			hideBackGround.SetActive(true);
			// PopIn Dialog
			UITransition.PopIn(dialogFinishGame.transform);
		} else {
			hideBackGround.SetActive(false);
			// PopOut Dialog
			UITransition.PopOut(dialogFinishGame.transform);
		}
	}

	public void QuitGame()
	{
		Application.Quit ();
	}

	public void PlayOn()
	{
		ShowFinishDiaLog (false);
	}
}
