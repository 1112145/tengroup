﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GamePlayController : MonoBehaviour {

	#region public zone
	public Timer timer;
	public GameObject hideBackGround;
	public GameObject dialogFinishGame;
	public GameObject dialogLoseGame;
	public GameObject dialogPauseGame;
	public TestControllet songController;
	public GameObject panel;
	public GameObject startPanel;
	public Text currentNameSong;
	//public bool Lose = false;
	public bool lose = false;
	public Text txtScore;

	public Text txtScoreWin;
	public Text txtScoreLose;

	public enum GameState
	{
		running,
		stopped,
		pause,
	}
	#endregion

	#region private zone
	private string nameSong;
	private GameState gameState;
	private bool showDialogFlag = false;
	private bool showPauseGameFlag = false;
	private int score =  0;
	#endregion
	[HideInInspector]
	public List<GameObject> tiles = new List<GameObject>();
	private int TileHeight;
	private int TileWidth;
	Vector3 beforeTilePosition;
	private float lastWidth;
	Vector3 basicPosition; 
	private float basicWidth;

	public void StartGame()
	{
		startPanel.SetActive (false);	
		gameState = GameState.running;
		for (int i = 0; i < tiles.Count; i++) {
			tiles[i].GetComponent<Tile>().state = Tile.State.run;
		}
		timer.currentState = Timer.TimerState.Run;
	}

	public void IncreaseScore()
	{
		score++;
		RenderScore ();
	}

	#region Render Zone
	public void RenderScore()
	{
		txtScore.text = "SCORE: " + score.ToString();
	}
	#endregion

	public void GenerateTileMap()
	{
		//yield return null;
		GameObject tile = GameObject.Find ("Tiles");
		SetEventOnclick(ref tile, 0);
		basicPosition = tile.transform.localPosition;

		TileWidth = (int)((RectTransform)tile.transform).rect.width;
		lastWidth = TileWidth;
		basicWidth = TileWidth;
		TileHeight = (int)((RectTransform)tile.transform).rect.height;

		tiles.Add (tile);
		//songController.InitSong ("LittleStar");
		RandomSong ();
		songController.InitSong (nameSong);
		for (int i = 1; i < songController.unitsSound.Count; i++) {
			GameObject newTile = Instantiate(tile);
			newTile.transform.SetParent(panel.transform);
			newTile.transform.localScale = new Vector3(1f,1f,1f);
			beforeTilePosition = tiles[i -1].transform.localPosition;
			RandomPosition(ref newTile);
			SetEventOnclick(ref newTile, i);
			//newTile.transform.localPosition = new Vector3(beforeTilePosition.x + 100, beforeTilePosition.y,beforeTilePosition.z);
			tiles.Add(newTile);
		}
	}

	public void InitFirstTile()
	{
		//tile = GameObject.Find ("Tiles");
		SetEventOnclick(ref tile, 0);
		basicPosition = tile.transform.localPosition;
		
		TileWidth = (int)((RectTransform)tile.transform.GetChild(0)).rect.width;
		lastWidth = TileWidth;
		basicWidth = TileWidth;
		TileHeight = (int)((RectTransform)tile.transform.GetChild(0)).rect.height;
		
		tiles.Add (tile);
		//songController.InitSong ("LittleStar");
		RandomSong ();
		songController.InitSong (nameSong);
	}

	public GameObject tile;
	public void GenerateTileMapFromIndex(int startIndex, int endIndex)
	{
		//yield return null;

		for (int i = startIndex; i < endIndex; i++) {
			GameObject newTile = Instantiate(tile);
			newTile.transform.SetParent(panel.transform);
			newTile.transform.localScale = new Vector3(1f,1f,1f);
			beforeTilePosition = tiles[i -1].transform.localPosition;
			RandomPosition(ref newTile);
			SetEventOnclick(ref newTile, i);
			//newTile.transform.localPosition = new Vector3(beforeTilePosition.x + 100, beforeTilePosition.y,beforeTilePosition.z);
			tiles.Add(newTile);
		}
	}

	public IEnumerator GenerateSecondPartTileMap(int startIndex, int endIndex)
	{
		yield return new WaitForSeconds (0.00000001f);
		GenerateTileMapFromIndex (startIndex, endIndex);
	}

	void GenerateIntelligent ()
	{
		InitFirstTile ();
		GenerateTileMapFromIndex (1, 8);
		StartCoroutine (GenerateSecondPartTileMap (8, songController.unitsSound.Count));
	}

	void RandomSong ()
	{
		int idx = Random.Range (0, 2);
		if (idx == 0) {
			nameSong = "LittleStar";
		} else if (idx == 1) {
			nameSong = "Arirang";
		}
		currentNameSong.text = "Playing: " + nameSong;
	}

	void SetEventOnclick (ref GameObject newTile, int id)
	{
		Button btn = newTile.GetComponent<Button> ();
		int capture = id;
		btn.onClick.AddListener (() => Onclick(capture));
	}

	public void Onclick(int idx)
	{
		songController.OnclickPlayMusicButton ();
		Image img = tiles [idx].transform.GetChild(0).GetComponent<Image> ();
		tiles [idx].GetComponent<Tile> ().clicked = true;
		Color temp = img.color;
		temp.a=0.1f;
		img.color = temp;
	}

	void RandomPosition (ref GameObject newTile)
	{
		int y;
		do {
			int idx = Random.Range(0,4);
			y = (int) basicPosition.y - idx*TileHeight;
		} while(y == beforeTilePosition.y);
		newTile.transform.localPosition = new Vector3(beforeTilePosition.x + lastWidth, y,beforeTilePosition.z);	
		// random size
		int random = Random.Range (0, 4);
		if (random == 2) {
			newTile.transform.localScale = new Vector3 (1.5f, 1f, 1f);
			Image img = newTile.transform.GetChild(0).GetComponent<Image>();
			img.sprite = Resources.Load<Sprite>("Images/phimdai");
			lastWidth = basicWidth * 1.5f;
		} else {
			lastWidth = basicWidth;
		}

	}

	// Use this for initialization
	void Start () {
		Screen.orientation = ScreenOrientation.Landscape;
		gameState = GameState.stopped;
		//GenerateTileMap ();
		//GenerateIntelligent ();
		//StartCoroutine( GenerateTileMap ());
	}
	bool flagWin = false;
	bool flag = false;
	bool flagChangeSpeed = false;

	bool flagstart = false;
	// Update is called once per frame
	void Update () {
		if (flagstart == false) {
			GenerateIntelligent();
			flagstart = true;
		}
		OnTimerOffEvent ();
		OnBackPressEvent ();
		if (flag == false) {
			if (lose && timer.CurrentValue <= 30 && flagWin!= true) {
				ShowLoseDiaLog(true);
				flag = true;
			}
			else if(lose && timer.CurrentValue > 30)
			{
				if(showDialogFlag == false){
					ShowFinishDiaLog(true);
					showDialogFlag = true;
				}

			}

		}
		if (timer.CurrentValue % 15 == 0 && timer.CurrentValue != 0) {
			if(flagChangeSpeed == false)
			{

				speed += 0.75f;
				flagChangeSpeed = true;
				Debug.Log("Change speed: " + speed);
			}
		}
		else
		{
			flagChangeSpeed = false;
		}
			
	}
	float speed = 6f;
	public GameObject test;


	#region Event
	void OnTimerOffEvent ()
	{
		if (timer.currentState == Timer.TimerState.Stop) {
			if (showDialogFlag == false) {
				ShowFinishDiaLog (true);
				showDialogFlag = true;
			}
		}
		if (gameState == GameState.running) {
			test.transform.position += -test.transform.right * speed * Time.deltaTime;
		}
	}

	void OnBackPressEvent ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (gameState == GameState.stopped) {
				BackToMenu ();
			}
			if (gameState == GameState.running) {
				showPauseGameFlag = false;
				gameState = GameState.pause;
			}
			if (gameState == GameState.pause) {
				PauseGame ();
			}
		}
	}
	#endregion


	#region Dialog

	public void ShowFinishDiaLog(bool show)
	{
		txtScoreWin.text = score.ToString ();
		flagWin = true;
		PlayWinSound ();
		if (show) {
			hideBackGround.SetActive(true);
			// PopIn Dialog
			UITransition.PopIn(dialogFinishGame.transform);
		} else {
			hideBackGround.SetActive(false);
			// PopOut Dialog
			UITransition.PopOut(dialogFinishGame.transform);
		}
	}

	public void ShowPauseDiaLog(bool show)
	{
		if (show) {
			hideBackGround.SetActive(true);
			UITransition.PopIn(dialogPauseGame.transform);
		} else {
			hideBackGround.SetActive(false);
			UITransition.PopOut(dialogPauseGame.transform);
		}
	}

	public void ShowLoseDiaLog(bool show)
	{
		txtScoreLose.text = score.ToString ();
		PlayFailSound ();
		if (show) {
			hideBackGround.SetActive(true);
			UITransition.PopIn(dialogLoseGame.transform);
		} else {
			hideBackGround.SetActive(false);
			UITransition.PopOut(dialogLoseGame.transform);
		}
	}

	#endregion


	#region Game Basic Navigation
	public void PlayGame()
	{
		timer.currentState = Timer.TimerState.Run;
		gameState = GameState.running; 
	}

	void PauseGame ()
	{
		if (showPauseGameFlag == false) {
			timer.currentState = Timer.TimerState.Pause;
			// Show pause game dialog. Add later
			ShowPauseDiaLog(true);
			showPauseGameFlag = true;
		}
	}

	public void ResumeGame()
	{
		gameState = GameState.running;
		timer.currentState = Timer.TimerState.Run;
		ShowPauseDiaLog (false);
	}

	public void RePlay()
	{
		Application.LoadLevel (Application.loadedLevelName);
	}

	public void BackToMenu()
	{
		Application.LoadLevel ("MenuLandScape");
	}

	public void LoseGame()
	{
		if(timer.CurrentValue <= 30)
		{
			lose = true;
		}
		else
		{
			//timer.currentState = Timer.TimerState.Stop;
			//showDialogFlag = false;
			ShowFinishDiaLog(true);
			showDialogFlag = true;
		}
	}

	private void PlayFailSound()
	{
		AudioClip clip = Resources.Load<AudioClip> ("Sounds/FailPage");
		SoundPlayer player = GetComponent<SoundPlayer> ();
		player.PlayAudio (clip);
	}

	private void PlayWinSound()
	{
		AudioClip clip = Resources.Load<AudioClip> ("Sounds/UpgradePage");
		SoundPlayer player = GetComponent<SoundPlayer> ();
		player.PlayAudio (clip);
	}
	#endregion
}
